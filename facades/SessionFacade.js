const {
  User,
} = require("../models");
const {
  RequiredException,
  LoginFailedException,
} = require("../exceptions");
const {
  jwt,
} = require("../utils");

module.exports = {
  getUserByAuthentication,
  login,
};

async function getUserByAuthentication(authentication) {
  if (!authentication) return null;
  const {
    id,
    email,
    password,
  } = authentication;
  if (!id || !password || !email) return null;
  return User.findOne({
    where: {
      id,
      email,
      password,
    },
  });
}

async function login(email, rawPassword) {
  RequiredException.check("email", email);
  RequiredException.check("rawPassword", rawPassword);
  const user = await User.findOne({
    where: {
      email,
      password: User.hashPassword(rawPassword),
    },
  });
  if (!user) throw new LoginFailedException();
  return {
    token: jwt.encode({
      email,
      password: user.password,
      id: user.id,
    }),
  };
}
