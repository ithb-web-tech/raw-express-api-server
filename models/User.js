const { sha256 } = require("../utils");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      role: DataTypes.STRING,
      password: DataTypes.STRING,
      rawPassword: {
        type: DataTypes.VIRTUAL,
        set(val) {
          this.setDataValue("rawPassword", val);
          this.setDataValue("password", sha256(val));
        },
      },
    },
    {},
  );
  // User.associate = function associate(models) {
  // associations can be defined here
  // };
  Object.assign(User.prototype, {
    authenticationData,
  });

  User.hashPassword = function hashPassword(value) {
    return sha256(value);
  };

  function authenticationData() {
    return {
      id: this.id,
      email: this.email,
      password: this.password,
    };
  }

  return User;
};
