const {
  Router,
} = require("../modules");
const {
  RootController,
  SessionController,
} = require("../controllers");


const router = new Router();

router.get("/info", RootController.info);
router.post("/login", SessionController.login);
router.get("/me", SessionController.info);

module.exports = router;
