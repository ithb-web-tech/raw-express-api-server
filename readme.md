# ITHB Web Tech Api Server

## Prerequisites
1. NodeJS ^10
1. NPM ^6
1. PostgreSQL ^10

## How to install
Before this, you need to make database using your database explorer. Here is the example of the create new database:
![Create Database](docs/images/create-database.png)

1. Git clone from repository
1. run ```npm install```
1. copy `.env.example` to `.env`
1. open the `.env`
1. configure the values of the `DB_USERNAME`, `DB_PASSWORD`, and so on according to your database configuration
1. for doing some test whether the setup is already correct or not, execute ```npm run test```

## Development mode
1. Run ```npm run watch```