module.exports = {
  app: {
    tokenDuration: 30 * 24 * 60 * 60, // 1 month
  },
  database: {
    username: "developer",
    password: "developer",
    database: "ithb-web-tech",
    host: "127.0.0.1",
    port: 5432,
    dialect: "postgres",
    logging: false,
  },
};
